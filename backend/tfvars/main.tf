resource "docker_network" "mynetwork" {
  name = var.network_name
}

resource "docker_container" "backend" {
  name    = "backend"
  image   = var.backend_image
  restart = "always"

  ports {
    internal = var.backend_internal_port
    external = var.backend_external_port
  }

  networks_advanced {
    name = docker_network.mynetwork.name
  }
}
