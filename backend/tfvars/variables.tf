variable "backend_image" {
  description = "Nome da imagem do contêiner backend"
  default     = "registry.gitlab.com/thlinux_th/backen-api-ci:latest"
}

variable "backend_internal_port" {
  description = "Porta interna do Backend"
  default     = 3000
}

variable "backend_external_port" {
  description = "Porta externa do Backend"
  default     = 3000
}

variable "network_name" {
  description = "Nome da rede Docker"
  default     = "mynetwork"
}